# Chrome extension continuous delivery with Bitbucket Pipelines

This is an example of a basic configuration setup for a chrome extension project that uses Bitbucket Pipelines to automate deployments to the Chrome Web Store.

This guide makes a few assumptions…

* You have a Google Cloud Platform account and are familiar with the Cloud console
* You have a Bitbucket account and the chrome extension source code is in a repository
* You’ve already uploaded a chrome extension to the Chrome Web Store

Note: It's not a problem if you don't use Bitbucket Pipelines. What's covered here can be modified to work with whichever continuous delivery tool you prefer.

## Preparation

First, take care of a few boilerplate operations. You’ll need to enable the **Chrome Web Store API** and generate oauth credentials (choose other/installed) from the Cloud console. Take note of the **client id** and **client secret** for later.

Next, open the following url in a _browser_ and replace **$CLIENT_ID** with the actual client id.

```
https://accounts.google.com/o/oauth2/auth?response_type=code&scope=https://www.googleapis.com/auth/chromewebstore&client_id=$CLIENT_ID&redirect_uri=urn:ietf:wg:oauth:2.0:oob
```

A **code** will be returned.

![](img/code.png)

Use the client id, client secret, and code to generate a **refresh token** in a _terminal_. Save for later.

```sh
$ curl "https://accounts.google.com/o/oauth2/token" -d \
"client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&code=$CODE&grant_type=authorization_code&redirect_uri=urn:ietf:wg:oauth:2.0:oob" | jq ".refresh_token"
```

The last thing you need is the chrome extension’s app id. Go to the Chrome Web Store developer dashboard and open the **More info** modal to find its id. Save for later.

_Remember, this guide assumes you’ve already uploaded the chrome extension to the Chrome Web Store._ 

![](img/app-id.png)

## Pipelines basic setup

The basic function of this automated deployment is to _update_ a chrome extension that already exists in the Chrome Web Store. You’ll identify the target extension by the app id.

See `bitbucket-pipelines.yml` for the basic setup.  The interesting part starts after the **script** keyword. For a detailed review of the Pipelines configuration file see [the documentation](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html).

_You’ll see a few placeholders in the following scripts ($CLIENT_ID, $CLIENT_SECRET, etc). The actual values for these will be provided by_ **environment variables** _discussed later in the guide._

```yml
...
script:
	- apt-get update
	- apt-get -y install jq zip
	- FILE_NAME=crx.zip
	- zip -r $FILE_NAME ./app
	- ACCESS_TOKEN=$(curl "https://accounts.google.com/o/oauth2/token" -d "client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&refresh_token=$REFRESH_TOKEN&grant_type=refresh_token&redirect_uri=urn:ietf:wg:oauth:2.0:oob" | jq -r '.access_token')
	- curl -H "Authorization:Bearer $ACCESS_TOKEN" -H "x-goog-api-version:2" -X PUT -T $FILE_NAME -v "https://www.googleapis.com/upload/chromewebstore/v1.1/items/$APP_ID"
	- curl -H "Authorization:Bearer $ACCESS_TOKEN" -H "x-goog-api-version:2" -H "Content-Length:0" -H "publishTarget:trustedTesters" -X POST -v "https://www.googleapis.com/chromewebstore/v1.1/items/$APP_ID/publish"
```

### Package

The Google Chrome Store API requires chrome extensions to be packaged as a zip file before upload. Although not necessary, it’s a good idea to include only the required files needed for the chrome extension to work. Exclude unit test files, the node_modules folder, unrelated configuration files, etc.

Place chrome extension specific files into a subfolder so that the zip command can be simple.

```sh
zip -r $FILE_NAME ./app
```

### Authorize

During the Pipelines build process, it gets an **access token** so that a call to the Google Chrome Store API can be made. The access token lasts for 40 minutes before it needs to be refreshed. This is where the refresh token comes into play.

```sh
ACCESS_TOKEN=$(curl "https://accounts.google.com/o/oauth2/token" -d "client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&refresh_token=$REFRESH_TOKEN&grant_type=refresh_token&redirect_uri=urn:ietf:wg:oauth:2.0:oob" | jq -r '.access_token')
```

### Upload

With the access token in hand, an authorized request to the API can be made to update the chrome extension.

```sh
curl \
-H "Authorization:Bearer $ACCESS_TOKEN" \
-H "x-goog-api-version:2" \
-X PUT \
-T master.zip \
-v "https://www.googleapis.com/upload/chromewebstore/v1.1/items/$APP_ID"
```

### Publish

To publish the chrome extension right away include the appropriate request.

```sh
curl \
-H "Authorization:Bearer $ACCESS_TOKEN" \
-H "x-goog-api-version:2" \
-H "Content-Length:0" \
-X POST \
-v "https://www.googleapis.com/chromewebstore/v1.1/items/$APP_ID/publish"
```

Or, publish to trusted testers only.

```sh
curl \
-H "Authorization:Bearer $ACCESS_TOKEN" \
-H "x-goog-api-version:2" \
-H "Content-Length:0" \
-H "publishTarget:trustedTesters" \
-X POST \
-v "https://www.googleapis.com/chromewebstore/v1.1/items/$APP_ID/publish"
```

## Environment variables

Back in Bitbucket, add the client id, client secret, refresh token, and app id you’ve collected as environment variables in the Pipelines settings section. The app id is publicly available, but you’ll want to secure the others so they aren’t compromised.

![](img/environment-variables.png)

## Other considerations 

Build a docker image to separate dependency installations. This could potentially save running time during the build process.

```dockerfile
FROM node:9

RUN apt-get update && apt-get install -y \
  jq
```